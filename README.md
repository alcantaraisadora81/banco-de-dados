# Atividades da disciplina Banco de Dados
Este repositório contém as atividades desenvolvidas por Isadora para a disciplina de Banco de Dados.

## Estrutura do Repositório
O repositório está estruturado da seguinte forma:

## Atividades: nesta pasta estão as atividades desenvolvidas por Isadora, organizadas por número e nome da atividade.
Projetos: nesta pasta estão os projetos desenvolvidos por Isadora, organizados por nome do projeto.
README.md: este arquivo contém informações gerais sobre o repositório e sua estrutura.
Desenvolvedora
Isadora

## Contato
Email: [alcantaraisadora81@gmail.com]
