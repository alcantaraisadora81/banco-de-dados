-- Isadola 

--- Questão 1 ---
CREATE DATABASE exercicio_insecaodedados;


CREATE TABLE alunos(
	idAluno INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	nomeAluno VARCHAR (50) NOT NULL ,
	sobreAluno VARCHAR (50) NOT NULL ,
	idadeAluno INT (2),
	enderecoAluno VARCHAR (50)

);
INSERT INTO alunos (nomeAluno,sobreAluno,idadeAluno,enderecoAluno)
	VALUES ('João', 'Pedro','20','Rua dos Bobos,16');
	
SELECT * FROM alunos;	


--- Questão 2 ---

exercicio_insecaodedadosCREATE TABLE categorias (
	idCateg INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	nomeCategoria  VARCHAR (50) NOT NULL 
);

INSERT INTO categorias (nomeCategoria)
	VALUES ('Eletrônicos'),
	('Livros'),
	('Brinquedos');

SELECT * FROM categorias ;


---  Questão 3 ---


CREATE TABLE livros  (
	idLivro INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	titulo VARCHAR (50) NOT NULL ,
	autor VARCHAR (50) NOT NULL ,
	editora VARCHAR (50) NOT NULL ,
	anodePlublicacao INT (4)
);


INSERT INTO livros (titulo,autor,editora,anodePlublicacao )
	VALUES ('Dom Casmurru','Machado de Assis','Nova Fronteira','1899'),
	('O pequeno príncipe','Antoine de Saint-Exupéry','Agir','1943');

SELECT * FROM livros;


--- Questão 4 ---

CREATE TABLE funcionario (
	idFuncionario INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	nomeFunc VARCHAR (50) NOT NULL ,
	cargo VARCHAR (50) NOT NULL ,
	salario DECIMAL(8,2)  NOT NULL 
);

INSERT INTO funcionario (nomeFunc,cargo,salario)
 VALUES ('Carlos', 'Gerente de projetos',8000.00 ),
 ('Ana', 'Analista de sistemas',5000.00),
 ('Marta', 'Desenvolvedor Web',3500.00 ),
 ('Bruna', 'Analista de RH', 4000 ),
 ('Felipe', 'Gerente Comercial',8000.00 ),
 ('Juliana', 'Progamadora Java',5000.00 );
 
SELECT * FROM funcionario;
 
 
 
--- Questão 5 ---

CREATE TABLE cliente (
	idCliente INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	nomeCliente VARCHAR (50) NOT NULL ,
	emailCliente VARCHAR (50),
	telefone CHAR (55) ,
	cidade VARCHAR (55)
);
 
INSERT INTO cliente (nomeCliente,emailCliente,telefone,cidade )
 VALUES ('Maria','maria@gmail.com','(11)99999-9999','Coreaú'),
 ('José','jose@gmail.com','(21)88888-8888','Alcântaras' ),
 ('Fernanda','fernanda@gmail.com','(11)98888-8888','Moraújo'),
 ('Rafael','rafael@gmail.com','(21)9777-7777','Sobral');
 
SELECT * FROM cliente;
 
--- Questão 6 ---

CREATE TABLE produtos (
	idProduto INTEGER  NOT NULL AUTO_INCREMENT  PRIMARY KEY ,
	nomeProduto VARCHAR (50) NOT NULL ,
	descProduto VARCHAR (50),
	precoProduto FLOAT (10),
	qtdproduto INT (199)
);

INSERT INTO produtos (nomeProduto,descProduto,precoProduto,qtdproduto )
 VALUES ('Mouse sem fio','Mouse ópitco sem fio com receptor USB','50.00',25),
  ('Teclado USB','Teclado USB com teclas macias e silenciosas ','35.00',15),
  ('Fone de ouvido ','Fone de ouvido com microfone e controle de volume','25.00',10),
  ('Monitor LED 19','Monitor LED 19 widescreen com resolução HD','450.00',10),
  ('Impressora Multifuncioanl ','mpressora Multifuncioanl jato de tinta com Wi-Fi','250.00',5),
  ('HD externo 1TB','HD externo 1TB USB 3.0 pórtatil','300.00',25);

SELECT * FROM produtos ;


--- Atividade ATUALIZAÇÃO DE DADOS --- 


--- Questão 01

UPDATE cliente SET telefone = '(11) 97777-7777'
WHERE idCliente = '1';

--- Questão 02

UPDATE produtos SET precoProduto = '299.00'
WHERE idproduto = '2';

--- Questão 03

UPDATE funcionario SET cargo = 'Supervisor de Vendas'
WHERE idFuncionario = '2';

SELECT * FROM funcionario ;

-- Questão 04 

CREATE TABLE pedido (
	idPedido INT (10) AUTO_INCREMENT  ,
	idCliente CHAR (11),
	idProd INT (3),
	qtdProduto INT (10) NOT NULL ,
	precoProduto FLOAT (10),
	PRIMARY KEY (idPedido),
	
	CONSTRAINT pedido_cliente FOREIGN KEY (idCliente)
	REFERENCES cliente (idCliente),
	
	CONSTRAINT pedido_produtos FOREIGN KEY (idProd) 
	REFERENCES produtos (idProduto)	
);


--- Questão 05


UPDATE categorias SET nomeCategoria = 'Jogos e Brinquedos'
WHERE idCateg = '3';

SELECT * FROM categorias ;


--- Questão 06 

UPDATE cliente SET emailCliente = 'refael@outlook.com'
WHERE idCliente= '4';

SELECT * FROM cliente ;

--- questão 07

UPDATE produtos SET descProduto='Monitor LED 19', precoProduto='499.00'
WHERE idProduto = '4'; 

SELECT * FROM produtos ;


--- questão 08 

UPDATE funcionario SET cargo = 'Analista de Recursos Humanos', salario = '499.00'
WHERE idFuncionario = '1';

SELECT * FROM funcionario ;


-- Questão 09

UPDATE categorias SET nomeCategoria = 'Literatura'
WHERE idCateg = 2;








